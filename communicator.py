#!/usr/bin/python3
import serial
import omr_converter
import socket

def determine_form(decoded_omr):
	# Hier bepalen we aan de hand van de eerste regel om welk document het gaat. En hiermee stellen we de hoeveelheid rijen in.
	if decoded_omr[1] == "0000001100000000000000000000000000000000":
		row_count=118
	elif decoded_omr[1] == "0000001010000000000000000000000000000000":
		row_count=54
	elif decoded_omr[1] == "0000101010000000000000000000000000000000":
		row_count=118
	elif decoded_omr[1] == "0000001010000110000000000000000000000000":
		row_count=54
	elif decoded_omr[1] == "0000001010000100000000000000000000000000":
		row_count=54
	else:
		print('Unknown row count')
		print(decoded_omr[1])
		row_count=None
	return(row_count)

def hex_to_ascii_omr(encoded_hex_omr):
	try:
		decoded_hex_omr = omr_converter.decoder_hex_omr(encoded_hex_omr)
	except:
		print('Decode error')
		return(None)
	if decoded_hex_omr:
		print(decoded_hex_omr)
		try:
			row_count = determine_form(decoded_hex_omr)
			encodedascii = omr_converter.encoder_ascii_omr(decoded_hex_omr, row_count)
		except:
			print('Encode error')
			return(None)
		return(encodedascii)
	else:
		return(None)




ser = serial.Serial()
#ser.port = "/dev/ttyUSB0"
ser.port = "/dev/ttyS0"
ser.baudrate = 9600
ser.bytesize = serial.SEVENBITS #number of bits per bytes
ser.parity = serial.PARITY_EVEN #set parity check: no parity
ser.stopbits = serial.STOPBITS_ONE #number of stop bits
ser.timeout = 10          #block read
#ser.xonxoff = False     #disable software flow control
#ser.rtscts = False     #disable hardware (RTS/CTS) flow control
#ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
#ser.writeTimeout = 2     #timeout for write


try:
    ser.open()
except(Exception, e):
    print("error open serial port:", str(e))
    exit()

# set port number
port = 4001

serversock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
serversock.bind(('::', port))
serversock.listen(5)



while True:
	(clientsock, address) = serversock.accept()
	print('received request from:', address)
	sock_receive = clientsock.recv(1024)
	if not sock_receive:
		clientsock.close()
		continue
	if sock_receive.decode()[:1] == 'L':
		# Send L to machine
		ser.write(sock_receive)
		# Read data from machine and transform
		printer_receive = ser.readline()
		if not printer_receive:
			clientsock.close()
			continue

		encodedascii = hex_to_ascii_omr(printer_receive.decode())
		if encodedascii:
			clientsock.send(encodedascii.encode())
		else:
			clientsock.close()
			continue
		# Read back G or S
		sock_receive = clientsock.recv(1024)
		if sock_receive.decode()[:1] == 'G' or sock_receive.decode()[:1] == 'S':
			# Send G or S to machine
			ser.write(sock_receive)
	elif sock_receive.decode()[:1] == 'G' or sock_receive.decode()[:1] == 'S':
		print('Out of sequence command received, passing on:', str(sock_receive))
		ser.write(sock_receive)
	else:
		print('Unknown command:', str(sock_receive))
	clientsock.close()

