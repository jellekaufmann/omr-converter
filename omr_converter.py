#!/usr/bin/python3

import sys
import re


# Receives a string from omr scanner that starts with \x02 (start of text) and ends with \x03 (end of text)
# Returns a dict with rownumbers and rowdata
def decoder_ascii_omr(scanresult):
	row = ""
	outputdict = {}
	rownumber=1
	# Check for barcode
	pattern = r'"([0-9]*)"'
	find_barcode = re.search(pattern, scanresult)
	if find_barcode:
		outputdict['barcode'] = find_barcode.group(1)
		scanresult = scanresult.replace("\"" + find_barcode.group(1) + "\"", "", 1)
	if scanresult[:1] != "\x02" or scanresult[-1:] != "\x03":
		raise ValueError("String prefix does not equal x02 (Start of text) or string suffix does not equal x03 (End of text). Input is invalid")
	else:
		# Strip Start of text/End of text
		scanresult = scanresult[1:-1]
	for character in scanresult:
		row+=bin(int.from_bytes(character.encode(), sys.byteorder))[3:]
		if len(row) == 42:
			# Strip the last two bits from the string (42 to 40 bits)
			row = row[:40]
			if row != "0000000000000000000000000000000000000000":
				outputdict[rownumber] = row
			row = ""
			rownumber+=1
	if row:
		raise ValueError("Inclomplete row. Input is invalid")
	else:
		return(outputdict)

# Receives a string from omr scanner including CRLF
# Returns a dict with rownumbers and rowdata
def decoder_hex_omr(scanresult):
	pagenumber = 0
	outputdict = {}
	# Check for M00/M99 error codes
	if len(scanresult) == 5 and scanresult[:1] == 'M':
		print("Error code received, skipping")
		return(None)
	# Remove the letter R. This signifies the start of rows read from the other side of the paper. This is not used in the other format
	scanresult = scanresult.replace("R", "", 1)
	if scanresult[-2:] == '\r\n':
		scanresult = scanresult[:-2]
	else:
		raise ValueError("Input doesn't have CRLF suffix")
	# Check for barcode
	pattern = r'"([0-9]*)"'
	find_barcode = re.search(pattern, scanresult)
	if find_barcode:
		outputdict['barcode'] = find_barcode.group(1)
		scanresult = scanresult.replace("\"" + find_barcode.group(1) + "\"", "", 1)
	# Check if the character are hexadecimal
	if scanresult.strip('0123456789ABCDEF'):
		raise ValueError("String contains unexpected characters.")
	# Split the string into values of 12 characters each  (page numer(2)+text(10))
	splits=[scanresult[x:x+12] for x in range(0,len(scanresult),12)]
	for row in splits:
		print(row)
		if len(row) == 12:
			rowoutput = ""
			rowoutput = ( bin(int(row[2:], 16)))[2:].zfill(40)
			if len(rowoutput) != 40:
				raise ValueError("Incomplete row. Input in invalid")
			else:
				# You are not going to believe this, if the pagecount goes over 99 it resets to 00
				if pagenumber > int(row[:2]):
					pagenumber = int(row[:2]) + 100
				else:
					pagenumber = int(row[:2])
				outputdict[pagenumber] = rowoutput
		else:
			raise ValueError("Incomplete row. Input in invalid")
	return(outputdict)

# Receives a dict and returns an encoded string
def encoder_ascii_omr(decodeddict, rowcount=None):
	outputstring = "\x02" # ASCII Start of text
	# Check for barcode
	if 'barcode' in decodeddict:
		barcode = decodeddict['barcode']
		del decodeddict['barcode']
	else:
		barcode = None
	# Als er geen rowcount is meegegeven het hoogste rijnummer gebruiken uit de dict
	if not rowcount:
		rowcount = max(decodeddict)
	for rownumber in range(1, rowcount + 1):
		if rownumber in decodeddict:
			if len(decodeddict[rownumber]) == 40:
				# Add a two bit suffix because this format is 42 bits longs
				row = decodeddict[rownumber] + '00'
			else:
				raise ValueError("Row is not 40 characters long. Invalid input")
		else:
			# 42 zeroes when it is an empty row
			row = "000000000000000000000000000000000000000000"
		# Split the row into grous of characters
		lijstvanbytes = [row[x:x+6] for x in range(0,len(row),6)]
		for karakter in lijstvanbytes:
			# Add a 1 bit prefix to make it a printable ascii character
			n = int('1' + karakter, 2)
			outputstring+=(n.to_bytes((n.bit_length() + 7) // 8, sys.byteorder).decode())
	if barcode:
		outputstring+="\"" + barcode + "\""
	outputstring+="\x03" # ASCII End of text
	return(outputstring)

# Receives a dict and returns an encoded string
def encoder_hex_omr(decodeddict):
	outputstring = ""
	# Check for barcode
	if 'barcode' in decodeddict:
		barcode = decodeddict['barcode']
		del decodeddict['barcode']
	else:
		barcode = None
	for rownumber, row in sorted(decodeddict.items()):
		if len(str(rownumber).zfill(2)) == 3:
			# When the row count goes over 100 it wraps around to 00
			rownumber = rownumber - 100
		outputstring+=str(rownumber).zfill(2)
		splits=[row[x:x+4] for x in range(0,len(row),4)]
		for character in splits:
			if len(character) == 4:
				outputstring+=hex(int(character, 2))[2:].upper()
			else:
				raise ValueError("Incomplete row. Input is invalid")
	if barcode:
		outputstring+="\"" + barcode + "\""
	outputstring+="\r\n"
	return(outputstring)
